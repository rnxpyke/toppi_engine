use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use crate::engine::{KNIGHT_MAP, PAWN_MAP_BLACK, PAWN_MAP_WHITE, ROOK_MAP_BLACK, ROOK_MAP_WHITE};

fn sqr_tbl_eval(
    board: &chess::Board,
    white: &[i8; 64],
    black: &[i8; 64],
    piece: chess::Piece,
) -> i16 {
    let pieces = board.pieces(piece);
    let mut score: i16 = 0;
    for pos in pieces & board.color_combined(chess::Color::White) {
        score += white[pos.to_int() as usize] as i16;
    }
    for pos in pieces & board.color_combined(chess::Color::Black) {
        score -= black[pos.to_int() as usize] as i16;
    }
    return score;
}

fn eval_objective(board: &chess::Board) -> i16 {
    let mut score: i16 = 0;
    for sqr in *board.combined() {
        let piece = board.piece_on(sqr).unwrap();
        let color = board.color_on(sqr).unwrap();
        let value = match piece {
            chess::Piece::Pawn => 100,
            chess::Piece::Knight => 300,
            chess::Piece::Bishop => 300,
            chess::Piece::Rook => 450,
            chess::Piece::Queen => 900,
            chess::Piece::King => 0,
        };
        match color {
            chess::Color::White => score += value,
            chess::Color::Black => score -= value,
        }
    }
    score += sqr_tbl_eval(board, &PAWN_MAP_WHITE, &PAWN_MAP_BLACK, chess::Piece::Pawn);
    score += sqr_tbl_eval(board, &ROOK_MAP_WHITE, &ROOK_MAP_BLACK, chess::Piece::Rook);
    score += sqr_tbl_eval(board, &KNIGHT_MAP, &KNIGHT_MAP, chess::Piece::Knight);
    if (board.pieces(chess::Piece::Bishop) & board.color_combined(chess::Color::White)).popcnt()
        == 2
    {
        score += 25;
    }
    if (board.pieces(chess::Piece::Bishop) & board.color_combined(chess::Color::Black)).popcnt()
        == 2
    {
        score -= 25;
    }

    return score;
}

fn eval(board: &chess::Board) -> i16 {
    let objective = eval_objective(board);
    match board.side_to_move() {
        chess::Color::White => objective,
        chess::Color::Black => -objective,
    }
}

pub struct MoveIter {
    iter: chess::MoveGen,
    stage: MoveIterStage,
}
pub enum MoveIterStage {
    Captures,
    Quiet,
}

impl Iterator for MoveIter {
    type Item = chess::ChessMove;

    fn next(&mut self) -> Option<Self::Item> {
        if let MoveIterStage::Captures = self.stage {
            match self.iter.next() {
                Some(m) => return Some(m),
                None => {
                    self.iter.set_iterator_mask(!chess::EMPTY);
                    self.stage = MoveIterStage::Quiet;
                }
            }
        }
        self.iter.next()
    }
}
fn moves(board: &chess::Board) -> impl Iterator<Item = chess::ChessMove> {
    let iterable = chess::MoveGen::new_legal(board);
    MoveIter {
        iter: iterable,
        stage: MoveIterStage::Captures,
    }
}

#[derive(Copy, Clone)]
pub struct TNode {
    depth: i16,
    ply: i16,
    score: i16,
    board: chess::Board,
    best_move: Option<chess::ChessMove>,
}

pub struct TT {
    table: Vec<Option<TNode>>,
}

impl TT {
    fn insert(&mut self, board: &chess::Board, node: TNode) {
        let hash = board.get_hash();
        let idx = hash as usize % self.table.len();
        self.table[idx] = Some(node);
    }

    fn get(&self, board: &chess::Board) -> Option<TNode> {
        let hash = board.get_hash();
        let idx = hash as usize % self.table.len();
        let node = self.table[idx];
        if let Some(inner) = node {
            if &inner.board == board {
                return node;
            }
        }
        return None;
    }
}

const KILLER_MOVES: usize = 2;

pub struct SearchRefs {
    pub nodes: usize,
    pub beta_cutoffs: usize,
    pub tt_hit: usize,
    pub stop: Arc<AtomicBool>,
    killer: [[Option<chess::ChessMove>; KILLER_MOVES]; 22],
    tt: TT,
}

impl SearchRefs {
    pub fn with_stop(stop: Arc<AtomicBool>) -> Self {
        let mut v = Self::default();
        v.stop = stop;
        return v;
    }
}

impl Default for SearchRefs {
    fn default() -> Self {
        Self {
            nodes: 0,
            beta_cutoffs: 0,
            tt_hit: 0,
            killer: [[None; KILLER_MOVES]; 22],
            stop: Arc::new(AtomicBool::new(false)),
            tt: TT {
                table: vec![None; 4194304],
            },
        }
    }
}

pub fn search(
    depth: i16,
    ply: i16,
    alpha: i16,
    beta: i16,
    board: chess::Board,
    refs: &mut SearchRefs,
) -> (i16, Option<chess::ChessMove>) {
    let mut best_eval = i16::MIN + 1;
    let mut best_move = None;
    for i in 1..=depth {
        let (eval, mov) = alphabeta(i, ply, alpha, beta, board, refs);
        best_eval = eval;
        best_move = mov;
        if let Some(m) = best_move {
            tracing::info!("{:?} {:?} {}", i, best_eval, m);
        } else {
            tracing::info!("{:?} {:?}", i, best_eval);
        }
        if refs.stop.load(Ordering::Acquire) {
            break;
        }
        if best_eval.abs() > 30000 {
            return (best_eval, best_move);
        }
    }
    return (best_eval, best_move);
}

pub const MVV_LVA: [[u16; 6 + 1]; 6 + 1] = [
    [0, 0, 0, 0, 0, 0, 0],       // victim None, attacker K, Q, R, B, N, P, None
    [0, 15, 14, 13, 12, 12, 10], // victim P, attacker K, Q, R, B, N, P, None
    [0, 25, 24, 24, 22, 21, 20], // victim K, attacker K, Q, R, B, N, P, None
    [0, 35, 34, 33, 32, 31, 30], // victim B, attacker K, Q, R, B, N, P, None
    [0, 45, 44, 43, 42, 41, 40], // victim R, attacker K, Q, R, B, N, P, None
    [0, 55, 54, 53, 52, 51, 50], // victim Q, attacker K, Q, R, B, N, P, None
    [0, 0, 0, 0, 0, 0, 0],       // victim K, attacker K, Q, R, B, N, P, None
];

pub fn alphabeta(
    mut depth: i16,
    ply: i16,
    mut alpha: i16,
    beta: i16,
    board: chess::Board,
    refs: &mut SearchRefs,
) -> (i16, Option<chess::ChessMove>) {
    refs.nodes += 1;
    if depth <= 0 {
        return (eval(&board), None);
    }
    if ply > 22 {
        return (eval(&board), None);
    }

    if board.checkers() != &chess::EMPTY {
        depth += 1;
    }

    let mut best_eval = i16::MIN + 1;
    let mut best_move: Option<chess::ChessMove> = None;
    let tt_move = {
        if let Some(node) = refs.tt.get(&board) {
            refs.tt_hit += 1;
            let tt_move = node.best_move.unwrap();
            if node.depth >= depth {
                return (node.score, Some(tt_move));
            }
            Some(tt_move)
        } else {
            None
        }
    };
    let moves: Vec<(chess::ChessMove, u16)> = if depth > 0 {
        let mut moves: Vec<_> = moves(&board)
            .map(|m| {
                if Some(m) == tt_move {
                    return (m, 100);
                }
                if let Some(killer) = refs.killer.get(ply as usize) {
                    if killer.contains(&Some(m)) {
                        return (m, 9);
                    }
                }
                let dest_idx = match board.piece_on(m.get_dest()) {
                    Some(p) => 1 + p.to_index(),
                    None => 0,
                };
                let src_idx = match board.piece_on(m.get_source()) {
                    Some(p) => 1 + p.to_index(),
                    None => 0,
                };
                let score = MVV_LVA[dest_idx][src_idx];
                (m, score)
            })
            .collect();
        moves.sort_by_key(|(_m, s)| -(*s as i16));
        moves
    } else {
        moves(&board).map(|m| (m, 0)).collect()
    };
    for (mov, _) in moves {
        let new_board = board.make_move_new(mov);
        let (score, _cont) = alphabeta(depth - 1, ply + 1, -beta, -alpha, new_board, refs);
        let selfish = -score;
        if selfish > best_eval {
            best_eval = selfish;
            best_move = Some(mov);
        }
        if best_eval >= beta {
            refs.beta_cutoffs += 1;
            if board.combined().popcnt() == new_board.combined().popcnt() {
                if let Some(killers) = refs.killer.get_mut(ply as usize) {
                    if killers[0] != Some(mov) {
                        for i in 1..KILLER_MOVES {
                            killers[i] = killers[i - 1];
                        }
                        killers[0] = Some(mov);
                    }
                }
            }
            return (beta, None);
        }
        if best_eval > alpha {
            alpha = best_eval;
        }
        if ply == 0 && refs.stop.load(Ordering::Acquire) {
            break;
        }
    }
    if best_move.is_none() {
        if board.checkers() == &chess::EMPTY {
            return (0, None);
        } else {
            return (-32000 + ply, None);
        }
    }
    refs.tt.insert(
        &board,
        TNode {
            depth,
            ply,
            best_move,
            board,
            score: best_eval,
        },
    );
    return (best_eval, best_move);
}
