use regex::Regex;
use std::str::FromStr;
use toppi_engine::abc;
use tracing_subscriber::{prelude::*, EnvFilter};

pub static benchmarks: &'static str = include_str!("eret.txt");
fn main() {
    let env_filter = EnvFilter::from_default_env();
    let fmt_layer = tracing_subscriber::fmt::layer()
        .without_time()
        .with_filter(env_filter);
    tracing_subscriber::registry().with(fmt_layer).init();
    let re = Regex::new("(.*) bm (.*); id \"(.*)\";").unwrap();
    let mut passes = 0;
    let mut fails = 0;
    for benchmark in benchmarks.lines() {
        let Some(caps) = re.captures(benchmark) else { continue };
        let fen = caps.get(1).unwrap().as_str();
        let id = caps.get(3).unwrap().as_str();
        let board = chess::Board::from_str(fen).unwrap();
        let bm = chess::ChessMove::from_san(&board, caps.get(2).unwrap().as_str()).unwrap();
        let mut refs = abc::SearchRefs::default();
        let (score, mov) = abc::search(10, 0, i16::MIN + 1, i16::MAX, board, &mut refs);
        if Some(bm) == mov {
            passes += 1;
            println!("{id}: passed");
        } else {
            fails += 1;
            println!("{id}: failed");
        }
    }
    println!("{passes} passed, {fails} failed");
}
