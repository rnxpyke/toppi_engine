use regex::Regex;
use std::str::FromStr;
use toppi_engine::abc;
use tracing_subscriber::{prelude::*, EnvFilter};

use std::time::Instant;
pub static PUZZLES: &'static str = include_str!("mate4.txt");

fn main() {
    let env_filter = EnvFilter::from_default_env();
    let fmt_layer = tracing_subscriber::fmt::layer()
        .without_time()
        .with_filter(env_filter);
    tracing_subscriber::registry().with(fmt_layer).init();
    let re = Regex::new(r"\n(.* - - .*)\n").unwrap();

    let start = Instant::now();
    for (i, m) in re.captures_iter(PUZZLES).enumerate() {
        let fen = m.get(1).unwrap().as_str();
        let board = chess::Board::from_str(fen).unwrap();
        let mut refs = abc::SearchRefs::default();
        let (score, mov) = abc::search(9, 0, i16::MIN + 1, i16::MAX, board, &mut refs);
        let per_puzzle = (Instant::now() - start) / (i + 1) as u32;
        println!("{}, mspp: {}", score, per_puzzle.as_millis());
    }
}
