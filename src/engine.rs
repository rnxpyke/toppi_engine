use std::{collections::BTreeMap, marker::PhantomData};

use chess::{Color, MoveGen, EMPTY};

use crate::{
    search::{Dynamics, Evaluator},
    traits::{Eval, SearchExtend, SelfishScore},
};

pub struct ChessDynamics;
pub struct PieceEvaluator;

pub struct FinalEval<E>(pub E);
impl<E: Eval<State = chess::Board, Score = SelfishScore<f64>>> Eval for FinalEval<E>
{
    type State = chess::Board;

    type Score = SelfishScore<f64>;

    fn eval(&self, state: &Self::State) -> Self::Score {
        let score = match state.status() {
            chess::BoardStatus::Ongoing => return self.0.eval(&state),
            chess::BoardStatus::Stalemate => 0.0,
            chess::BoardStatus::Checkmate => f64::NEG_INFINITY,
        };
        //tracing::info!("{:?} {}", state.status(), score);
        SelfishScore(score)
    }
}

#[derive(Copy, Clone)]
pub struct Objective(pub f64);
pub struct Selfish(pub f64);

impl AsRef<f64> for Selfish {
    fn as_ref(&self) -> &f64 {
        &self.0
    }
}

pub fn centipawn_score(board: chess::Board) -> SelfishScore<i32> {
    let mut score = 0;
    for sq in chess::ALL_SQUARES {
        let s = match board.piece_on(sq) {
            Some(chess::Piece::Pawn) => 100,
            Some(chess::Piece::Knight) => 300,
            Some(chess::Piece::Bishop) => 300,
            Some(chess::Piece::King) => 50,
            Some(chess::Piece::Queen) => 900,
            Some(chess::Piece::Rook) => 500,
            None => continue,
        };
        let s = match board.color_on(sq) {
            Some(chess::Color::White) => s,
            Some(chess::Color::Black) => -s,
            None => continue,
        };
        score += s;
    }
    match board.side_to_move() {
        chess::Color::White => SelfishScore(score),
        chess::Color::Black => SelfishScore(-score),
    }
}

pub fn centipawn_score_f64(board: chess::Board) -> SelfishScore<f64> {
    SelfishScore(centipawn_score(board).0 as f64)
}

#[test]
fn square_nums() {
    for square in chess::ALL_SQUARES {
        println!("square {} {}", square, square.to_int())
    }
}

#[rustfmt::skip]
pub static PAWN_MAP_WHITE: [i8; 64] = [
    0, 0, 10, 20, 30, 40, 50, 100,
    0, 0, 10, 20, 30, 40, 50, 100,
    0, 0, 10, 20, 30, 40, 50, 100,
    0, -10, 10, 20, 30, 40, 50, 100,
    0, -10, 10, 20, 30, 40, 50, 100,
    0, 0, 10, 20, 30, 40, 50, 100,
    0, 0, 10, 20, 30, 40, 50, 100,
    0, 0, 10, 20, 30, 40, 50, 100,
];

#[rustfmt::skip]
pub static PAWN_MAP_BLACK: [i8; 64] = [
    100, 50, 40, 30, 20, 10, 0, 0, // a1 -h1
    100, 50, 40, 30, 20, 10, 0, 0, // a2 -h2
    100, 50, 40, 30, 20, 10, 0, 0,
    100, 50, 40, 30, 20, 10, 0, -10,
    100, 50, 40, 30, 20, 10, 0, -10,
    100, 50, 40, 30, 20, 10, 0, 0,
    100, 50, 40, 30, 20, 10, 0, 0,
    100, 50, 40, 30, 20, 10, 0, 0,
];

#[rustfmt::skip]
pub static KNIGHT_MAP: [i8; 64] = [
    00, 10, 05, 20, 20, 50, 10, 00,
    10, 20, 20, 20, 20, 20, 20, 10,
    20, 20, 20, 30, 30, 20, 20, 20,
    20, 20, 30, 50, 50, 30, 20, 20,
    20, 20, 30, 50, 50, 30, 20, 20,
    20, 20, 20, 30, 30, 20, 20, 20,
    10, 20, 20, 20, 20, 20, 20, 10,
    00, 10, 05, 20, 20, 50, 10, 00,
];

pub static ROOK_MAP_WHITE: [i8; 64] = [
    10, 00, 00, 00, 00, 00, 40, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 10, 10, 10, 10, 10, 50, 30,
    10, 00, 00, 00, 00, 00, 40, 30,
];

pub static ROOK_MAP_BLACK: [i8; 64] = [
    30, 40, 00, 00, 00, 00, 00, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 50, 10, 10, 10, 10, 10, 10,
    30, 40, 00, 00, 00, 00, 00, 10,
];

pub struct EvalBuilder<T> {
    eval: T,
}


impl<T> EvalBuilder<T> {
    pub fn new() -> EvalBuilder<NullEval<T>> {
        EvalBuilder {
            eval: NullEval {
                _phantom: PhantomData,
            },
        }
    }
    pub fn layer<E>(self, eval: E) -> EvalBuilder<Layer<T, E>> {
        EvalBuilder {
            eval: Layer {
                a: self.eval,
                b: eval,
            },
        }
    }
}

pub struct Layer<A, B> {
    a: A,
    b: B,
}

impl<State, A, B> Eval for Layer<A, B>
where
    A: Eval<State = State, Score = SelfishScore<f64>>,
    B: Eval<State = State, Score = SelfishScore<f64>>,
{
    type State = State;

    type Score = SelfishScore<f64>;

    fn eval(&self, state: &Self::State) -> Self::Score {
        let a = self.a.eval(state);
        let b = self.b.eval(state);
        return SelfishScore(a.0 + b.0);
    }
}

impl<State, Score, T> Eval for EvalBuilder<T>
where
    T: Eval<State = State, Score = Score>,
{
    type State = State;

    type Score = Score;

    fn eval(&self, state: &Self::State) -> Self::Score {
        self.eval.eval(state)
    }
}

pub struct NullEval<S> {
    _phantom: PhantomData<S>,
}
impl<S> Eval for NullEval<S> {
    type State = S;
    type Score = SelfishScore<f64>;

    fn eval(&self, state: &Self::State) -> Self::Score {
        SelfishScore(0.0)
    }
}

pub struct PieceMap {
    pub white: &'static [i8; 64],
    pub black: &'static [i8; 64],
    pub piece: chess::Piece,
}
impl Eval for PieceMap {
    type State = chess::Board;
    type Score = SelfishScore<f64>;

    fn eval(&self, board: &Self::State) -> Self::Score {
        let mut score: i32 = 0;
        let bb = board.pieces(self.piece) | board.color_combined(Color::White);
        for pos in bb {
            score += self.white[pos.to_int() as usize] as i32;
        }
        let bb = board.pieces(self.piece) | board.color_combined(Color::Black);
        for pos in bb {
            score -= self.black[pos.to_int() as usize] as i32;
        }
        match board.side_to_move() {
            Color::White => return SelfishScore(score as f64),
            Color::Black => return SelfishScore(-score as f64),
        }
    }
}

pub struct CentipawnScore;
impl Eval for CentipawnScore {
    type State = chess::Board;
    type Score = SelfishScore<f64>;

    fn eval(&self, state: &Self::State) -> Self::Score {
        centipawn_score_f64(*state)
    }
}

impl Evaluator for PieceEvaluator {
    type Dyn = ChessDynamics;

    type Score = Objective;

    type PlayerScore = f64;

    fn evaluate(&self, board: &chess::Board) -> Self::Score {
        let mut score = 0.0;
        for sq in chess::ALL_SQUARES {
            let s = match board.piece_on(sq) {
                Some(chess::Piece::Pawn) => 1.0,
                Some(chess::Piece::Knight) => 3.0,
                Some(chess::Piece::Bishop) => 3.0,
                Some(chess::Piece::King) => 0.5,
                Some(chess::Piece::Queen) => 8.0,
                Some(chess::Piece::Rook) => 5.0,
                None => continue,
            };
            let s = match board.color_on(sq) {
                Some(chess::Color::White) => s,
                Some(chess::Color::Black) => -s,
                None => continue,
            };
            score += s;
        }
        return Objective(score);
    }

    fn interpret_score_for_player(&self, score: Objective, player: chess::Color) -> f64 {
        match player {
            chess::Color::White => score.0,
            chess::Color::Black => -score.0,
        }
    }
}

impl Dynamics for ChessDynamics {
    type State = chess::Board;
    type Player = chess::Color;
    type Move = chess::ChessMove;

    fn available_moves(&self, board: &Self::State) -> Vec<Self::Move> {
        let mut moves = vec![];

        let mut iterable = MoveGen::new_legal(&board);
        iterable.set_iterator_mask(*board.color_combined(!board.side_to_move()));
        for m in &mut iterable {
            moves.push(m);
        }
        iterable.set_iterator_mask(!EMPTY);
        for m in &mut iterable {
            moves.push(m);
        }
        return moves;
    }

    fn apply_move(&self, state: &Self::State, mov: &Self::Move) -> Self::State {
        state.make_move_new(*mov)
    }

    fn current_player(&self, state: &Self::State) -> Self::Player {
        state.side_to_move()
    }
}


#[derive(Default)]
pub struct NoExtend<S, M> { state: PhantomData<S>, mov: PhantomData<M> }
impl<S, M> SearchExtend for NoExtend<S, M> {
    type State = S;

    type Move = M;

    fn extend(&self, state: &Self::State, mov: &Self::Move) -> bool {
        false
    }
}

pub struct CaptureExtender;
impl SearchExtend for CaptureExtender {
    type State = chess::Board;

    type Move = chess::ChessMove;

    fn extend(&self, board: &Self::State, mov: &Self::Move) -> bool {
        let pieces = board.combined().count();
        let after = board.make_move_new(*mov).combined().count();
        let capture = pieces > after;
        if capture {
            return true;
        }
        return false;
    }
}
