use std::{
    collections::{HashMap, VecDeque},
    io::{self, BufRead},
    str::FromStr,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::RecvTimeoutError,
        Arc,
    },
    thread::JoinHandle,
    time::{Duration, Instant},
};

use toppi_engine::{
    engine::{
        CaptureExtender, CentipawnScore, ChessDynamics, EvalBuilder, FinalEval, NoExtend,
        PieceEvaluator, PieceMap, KNIGHT_MAP, PAWN_MAP_BLACK, PAWN_MAP_WHITE, ROOK_MAP_BLACK,
        ROOK_MAP_WHITE,
    },
    search::{self, AlphaBetaLookup, SearchContext, TranspositionTable},
};
use tracing_subscriber::{prelude::*, EnvFilter};
use vampirc_uci::{UciInfoAttribute, UciMessage, UciSearchControl, UciTimeControl};

pub struct SearchResult;

pub type EvalFn = fn(
    board: chess::Game,
    tx: UciSender,
    stop: Arc<AtomicBool>,
    tc: Option<UciTimeControl>,
    sc: Option<UciSearchControl>,
) -> eyre::Result<()>;
pub type UciSender = std::sync::mpsc::Sender<UciMessage>;
pub type UciReceiver = std::sync::mpsc::Receiver<UciMessage>;

pub struct UciServer {
    start_eval: EvalFn,
}

fn uci_parser(tx: UciSender) -> eyre::Result<()> {
    for line in io::stdin().lock().lines() {
        let msg: UciMessage = vampirc_uci::parse_one(&line?);
        tx.send(msg)?;
    }
    return Ok(());
}

impl UciServer {
    pub fn with_eval(f: EvalFn) -> Self {
        Self { start_eval: f }
    }

    pub fn uci_out(&self, msg: UciMessage) {
        println!("{}", msg);
    }

    pub fn launch(&mut self, tx: UciSender, rx: UciReceiver) -> eyre::Result<()> {
        let mut stop = Arc::new(AtomicBool::new(false));
        let mut game = chess::Game::new();
        let mut search: Option<JoinHandle<eyre::Result<()>>> = Option::None;
        for msg in rx.into_iter() {
            match msg {
                UciMessage::Uci => {
                    self.uci_out(UciMessage::Id {
                        name: Some("toopi-engine".into()),
                        author: Some("rnxpyke".into()),
                    });
                    self.uci_out(UciMessage::UciOk);
                }
                UciMessage::Position {
                    startpos,
                    fen,
                    moves,
                } => {
                    if startpos {
                        game = chess::Game::new();
                    }
                    if let Some(fen) = fen {
                        game = chess::Game::from_str(&fen.0).expect("invalid fen")
                    }
                    for m in moves {
                        game.make_move(m);
                    }
                }
                UciMessage::Debug(_) => {}
                UciMessage::IsReady => {
                    let search = search.take();
                    if let Some(handle) = search {
                        if !handle.is_finished() {
                            // TODO: send stop signal
                            let _ = handle.join().unwrap();
                        }
                    }
                    self.uci_out(UciMessage::ReadyOk);
                }
                UciMessage::Register { .. } => todo!(),
                UciMessage::SetOption { .. } => todo!(),
                UciMessage::UciNewGame => {}
                UciMessage::Stop => {
                    stop.store(true, Ordering::SeqCst);
                    stop = Arc::new(AtomicBool::new(false));
                }
                UciMessage::PonderHit => todo!(),
                UciMessage::Quit => {
                    stop.store(true, Ordering::SeqCst);
                    if let Some(handle) = search.take() {
                        handle.join().unwrap()?;
                    }
                    return Ok(());
                }
                UciMessage::Go {
                    time_control,
                    search_control,
                } => {
                    let prev_search = search.take();
                    if let Some(handle) = prev_search {
                        if !handle.is_finished() {
                            stop.store(true, Ordering::SeqCst);
                            handle.join().unwrap()?;
                        }
                    }
                    stop = Arc::new(AtomicBool::new(false));
                    let eval = self.start_eval.clone();
                    let tx = tx.clone();
                    let stop = stop.clone();
                    let game = game.clone();
                    search = Some(std::thread::spawn(move || {
                        eval(game, tx, stop, time_control, search_control)
                    }));
                }
                best @ UciMessage::BestMove { .. } => self.uci_out(best),
                info @ UciMessage::Info(_) => self.uci_out(info),
                m => panic!("Unexpected Message, {}", m),
            }
        }
        return Ok(());
    }
}

fn eval_basic(
    game: chess::Game,
    tx: UciSender,
    stop: Arc<AtomicBool>,
    tc: Option<UciTimeControl>,
    _sc: Option<UciSearchControl>,
) -> eyre::Result<()> {
    let (totx, torx) = std::sync::mpsc::channel();
    let search_tx = tx.clone();
    let search_stop = stop.clone();
    let board = game.current_position();
    let search: JoinHandle<eyre::Result<()>> = std::thread::spawn(move || {
        let mut res = VecDeque::new();
        let mut transpositions = TranspositionTable {
            table: HashMap::new(),
        };
        for depth in (0..15).step_by(1) {
            let eval = FinalEval(
                EvalBuilder::new()
                    .layer(CentipawnScore)
                    .layer(PieceMap {
                        white: &PAWN_MAP_WHITE,
                        black: &PAWN_MAP_BLACK,
                        piece: chess::Piece::Pawn,
                    })
                    .layer(PieceMap {
                        white: &KNIGHT_MAP,
                        black: &KNIGHT_MAP,
                        piece: chess::Piece::Knight,
                    })
                    .layer(PieceMap {
                        white: &ROOK_MAP_WHITE,
                        black: &ROOK_MAP_BLACK,
                        piece: chess::Piece::Rook,
                    }),
            );
            let ctx = SearchContext {
                game: ChessDynamics,
                eval,
                extend: NoExtend::default(),
                stop: stop.clone(),
            };
            let (score, moves) = search::search(&ctx, board, depth, &mut transpositions);
            search_tx.send(UciMessage::Info(vec![
                UciInfoAttribute::Depth(depth as u8),
                UciInfoAttribute::Pv(moves.iter().copied().collect()),
                UciInfoAttribute::Score {
                    cp: Some(score.0 as i32),
                    mate: None,
                    lower_bound: None,
                    upper_bound: None,
                },
            ]))?;
            if stop.load(Ordering::SeqCst) {
                break;
            }
            res = moves;
            if score.0 == f64::INFINITY {
                break;
            }
        }

        match totx.send(UciMessage::BestMove {
            best_move: res[0],
            ponder: None,
        }) {
            Ok(_) => return Ok(()),
            Err(_) => return Ok(()),
        }
    });

    let mut timeout = Duration::from_secs(60);
    if let Some(tc) = tc {
        match tc {
            UciTimeControl::TimeLeft {
                white_time: Some(wt),
                black_time: Some(bt),
                white_increment,
                black_increment,
                moves_to_go,
            } => {
                let ttg = match game.side_to_move() {
                    chess::Color::White => wt,
                    chess::Color::Black => bt,
                };
                let tenth = ttg / 10;
                let inc = match game.side_to_move() {
                    chess::Color::White => white_increment,
                    chess::Color::Black => black_increment,
                };
                match (inc, moves_to_go) {
                    (None, None) => timeout = tenth.to_std()?,
                    (None, Some(_)) => {}
                    (Some(inc), None) => timeout = (tenth + inc).to_std()?,
                    (Some(inc), Some(0)) => timeout = (tenth + inc).to_std()?,
                    (Some(inc), Some(x)) => timeout = (tenth + inc / x as i32).to_std()?,
                }
            }
            UciTimeControl::MoveTime(t) => timeout = t.to_std()?,
            _ => {}
        }
    }
    tracing::info!("timeout {:?}", timeout);
    match torx.recv_timeout(timeout) {
        Ok(msg) => {
            tx.send(msg)?;
            search.join().unwrap()?;
            return Ok(());
        }
        Err(RecvTimeoutError::Timeout) => {
            search_stop.store(true, Ordering::Release);
            let msg = torx.recv()?;
            tx.send(msg)?;
            search.join().unwrap()?;
            return Ok(());
        }
        Err(e) => {
            return Err(e)?;
        }
    }
}

fn eval_basic_other(
    game: chess::Game,
    tx: UciSender,
    stop: Arc<AtomicBool>,
    tc: Option<UciTimeControl>,
    _sc: Option<UciSearchControl>,
) -> eyre::Result<()> {
    let (totx, torx) = std::sync::mpsc::channel();
    let search_tx = tx.clone();
    let search_stop = stop.clone();
    let board = game.current_position();
    let search: JoinHandle<eyre::Result<()>> = std::thread::spawn(move || {
        let mut refs = toppi_engine::abc::SearchRefs::with_stop(stop.clone());
        let (score, mov) =
            toppi_engine::abc::search(15, 0, i16::MIN + 1, i16::MAX, board, &mut refs);
        match totx.send(UciMessage::BestMove {
            best_move: mov.unwrap(),
            ponder: None,
        }) {
            Ok(_) => return Ok(()),
            Err(_) => return Ok(()),
        }
    });

    let mut timeout = Duration::from_secs(60);
    if let Some(tc) = tc {
        match tc {
            UciTimeControl::TimeLeft {
                white_time: Some(wt),
                black_time: Some(bt),
                white_increment,
                black_increment,
                moves_to_go,
            } => {
                let ttg = match game.side_to_move() {
                    chess::Color::White => wt,
                    chess::Color::Black => bt,
                };
                let tenth = ttg / 10;
                let inc = match game.side_to_move() {
                    chess::Color::White => white_increment,
                    chess::Color::Black => black_increment,
                };
                match (inc, moves_to_go) {
                    (None, None) => timeout = tenth.to_std()?,
                    (None, Some(_)) => {}
                    (Some(inc), None) => timeout = (tenth + inc).to_std()?,
                    (Some(inc), Some(0)) => timeout = (tenth + inc).to_std()?,
                    (Some(inc), Some(x)) => timeout = (tenth + inc / x as i32).to_std()?,
                }
            }
            UciTimeControl::MoveTime(t) => timeout = t.to_std()?,
            _ => {}
        }
    }
    tracing::info!("timeout {:?}", timeout);
    match torx.recv_timeout(timeout) {
        Ok(msg) => {
            tx.send(msg)?;
            search.join().unwrap()?;
            return Ok(());
        }
        Err(RecvTimeoutError::Timeout) => {
            search_stop.store(true, Ordering::Release);
            let msg = torx.recv()?;
            tx.send(msg)?;
            search.join().unwrap()?;
            return Ok(());
        }
        Err(e) => {
            return Err(e)?;
        }
    }
}

fn main() -> eyre::Result<()> {
    color_eyre::install()?;
    let env_filter = EnvFilter::from_default_env();
    let fmt_layer = tracing_subscriber::fmt::layer()
        .without_time()
        .with_filter(env_filter);
    tracing_subscriber::registry().with(fmt_layer).init();
    tracing::info!("Hello, world!");

    let (tx, rx) = std::sync::mpsc::channel();
    let _ = std::thread::spawn({
        let tx = tx.clone();
        || uci_parser(tx)
    });
    UciServer::with_eval(eval_basic_other).launch(tx, rx)?;
    std::process::exit(0);
}
