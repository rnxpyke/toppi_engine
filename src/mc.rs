use std::{
    cell::{Cell, RefCell},
    rc::Rc,
};

use crate::{
    engine::{ChessDynamics, PieceEvaluator},
    search::{Dynamics, Evaluator},
};

#[derive(Debug)]
pub enum Node<S> {
    Expanded {
        state: S,
        total_score: f64,
        visits: u64,
        children: Vec<Node<S>>,
    },
    Leaf {
        state: S,
    },
    Terminal {
        state: S,
        total_score: f64,
        visits: u64,
    },
}

impl<S: Copy> Node<S> {
    fn state(&self) -> S {
        match self {
            Node::Expanded {
                state,
                total_score,
                visits,
                children,
            } => *state,
            Node::Leaf { state } => *state,
            Node::Terminal {
                state,
                total_score,
                visits,
            } => *state,
        }
    }
}

impl<S> Node<S> {
    fn new(state: S) -> Self {
        Self::Leaf { state }
    }

    fn avg_value(&self) -> f64 {
        match self {
            &Node::Expanded {
                total_score,
                visits,
                ..
            } => total_score / visits as f64,
            &Node::Leaf { .. } => f64::INFINITY,
            &Node::Terminal {
                total_score,
                visits,
                ..
            } => total_score / visits as f64,
        }
    }

    fn visits(&self) -> f64 {
        match self {
            &Node::Expanded { visits, .. } => visits as f64,
            &Node::Leaf { .. } => 0.0,
            &Node::Terminal { visits, .. } => visits as f64,
        }
    }

    fn ucb(&self, parent_visits: u64) -> f64 {
        let visits = self.visits();
        if visits == 0.0 {
            return f64::INFINITY;
        }
        let avg = self.avg_value();
        let ln = f64::ln(parent_visits as f64);
        let frac = ln / visits;
        let sqrt = f64::sqrt(frac);
        let sum = avg + sqrt;
        return sum;
    }
}

struct MctsOps<S> {
    expand: fn(S) -> Vec<S>,
    rollout: fn(S) -> f64,
}

fn playout<S: std::fmt::Debug + Copy>(cur: &mut Node<S>, ops: &MctsOps<S>) -> f64 {
    match cur {
        Node::Expanded {
            state,
            total_score,
            visits,
            children,
        } => {
            let next: &mut Node<S> = {
                let mut max = None;
                let mut next = None;
                for child in children.iter_mut() {
                    let ucb = child.ucb(*visits);
                    if ucb >= *max.get_or_insert(ucb) {
                        max = Some(ucb);
                        next = Some(child);
                    }
                }
                next.unwrap()
            };
            let score = playout(next, ops);
            *total_score += score;
            *visits += 1;
            score
        }
        &mut Node::Leaf { state } => {
            let children = (ops.expand)(state);
            let score = (ops.rollout)(state);
            if children.is_empty() {
                *cur = Node::Terminal {
                    state,
                    total_score: score,
                    visits: 1,
                };
            } else {
                *cur = Node::Expanded {
                    state,
                    total_score: score,
                    visits: 1,
                    children: children.into_iter().map(Node::new).collect(),
                };
            }
            score
        }
        Node::Terminal {
            state,
            total_score,
            visits,
        } => {
            let score = *total_score / *visits as f64;
            *total_score += score;
            *visits += 1;
            score
        }
    }
}

fn mcts_tree_search<S: ToString + std::fmt::Debug + Copy + 'static>(
    init_state: S,
    ops: &MctsOps<S>,
) {
    let mut root = Node::new(init_state);
    for i in 0..160000 {
        playout(&mut root, ops);
    }
    match root {
        Node::Expanded {
            state,
            total_score,
            visits,
            children,
        } => {
            for child in children.iter() {
                println!(
                    "visits: {} {}, {}",
                    child.visits(),
                    child.avg_value(),
                    child.state().to_string()
                );
            }
        }
        Node::Leaf { state } => todo!(),
        Node::Terminal {
            state,
            total_score,
            visits,
        } => todo!(),
    }
}

#[test]
fn tree_search() {
    fn expand(board: chess::Board) -> Vec<chess::Board> {
        ChessDynamics
            .available_moves(&board)
            .iter()
            .map(|&m| board.make_move_new(m))
            .collect()
    }
    fn rollout(board: chess::Board) -> f64 {
        PieceEvaluator.evaluate(&board).0
    }
    mcts_tree_search(
        chess::Board::default(),
        &MctsOps {
            expand: expand,
            rollout: rollout,
        },
    )
}
