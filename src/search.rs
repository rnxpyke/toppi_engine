use std::{
    collections::{HashMap, VecDeque},
    hash::Hash, sync::{atomic::{AtomicBool, Ordering}, Arc},
};

use crate::traits::{Eval, SearchExtend, SelfishScore};

pub trait Dynamics {
    type State;
    type Player;
    type Move;

    fn available_moves(&self, state: &Self::State) -> Vec<Self::Move>;
    fn apply_move(&self, state: &Self::State, mov: &Self::Move) -> Self::State;
    fn current_player(&self, state: &Self::State) -> Self::Player;
}

pub trait Evaluator {
    type Dyn: Dynamics;
    type Score;
    type PlayerScore;

    fn evaluate(&self, state: &<Self::Dyn as Dynamics>::State) -> Self::Score;
    fn interpret_score_for_player(
        &self,
        score: Self::Score,
        player: <Self::Dyn as Dynamics>::Player,
    ) -> Self::PlayerScore;

    fn eval_for(
        &self,
        state: &<Self::Dyn as Dynamics>::State,
        player: <Self::Dyn as Dynamics>::Player,
    ) -> Self::PlayerScore {
        let score = self.evaluate(&state);
        self.interpret_score_for_player(score, player)
    }
}

pub struct Minimax<D: Dynamics, E: Evaluator<Dyn = D>> {
    pub dynamics: D,
    pub evaluator: E,
}

impl<D: Dynamics, E: Evaluator<Dyn = D, PlayerScore = f64>> Minimax<D, E>
where
    D::Player: Copy,
    E::Score: Copy,
{
    pub fn boost(&self, board: D::State, depth: usize) -> (E::Score, VecDeque<D::Move>) {
        if depth <= 0 {
            return (self.evaluator.evaluate(&board), VecDeque::new());
        }
        let moves = self.dynamics.available_moves(&board);
        if moves.is_empty() {
            return (self.evaluator.evaluate(&board), VecDeque::new());
        }
        let player = self.dynamics.current_player(&board);

        let mut max = f64::NEG_INFINITY;
        let mut best_score = None;
        let mut best_moves = VecDeque::new();
        for m in moves {
            let (score, moves) = self.boost(self.dynamics.apply_move(&board, &m), depth - 1);
            let selfish = self.evaluator.interpret_score_for_player(score, player);
            if selfish > max {
                max = selfish;
                best_score = Some(score);
                best_moves = moves;
                best_moves.push_front(m);
            }
        }
        return (best_score.unwrap(), best_moves);
    }
}

pub struct AlphaBeta<D: Dynamics, E: Evaluator<Dyn = D>> {
    pub dynamics: D,
    pub evaluator: E,
}

impl<S, D, P, E> AlphaBeta<D, E>
where
    S: Copy,
    P: Copy,
    D: Dynamics<Player = P>,
    E: Evaluator<Dyn = D, Score = S, PlayerScore = f64>,
{
    fn alpha_beta(
        &self,
        board: &D::State,
        depth: usize,
        alpha: f64,
        beta: f64,
    ) -> (E::PlayerScore, VecDeque<D::Move>) {
        let player = self.dynamics.current_player(&board);
        if depth <= 0 {
            let score = self.evaluator.eval_for(&board, player);
            return (score, VecDeque::new());
        }
        let moves = self.dynamics.available_moves(&board);
        if moves.is_empty() {
            let score = self.evaluator.eval_for(&board, player);
            return (score, VecDeque::new());
        }

        let mut max = alpha;
        let mut best_moves = VecDeque::new();
        for m in moves {
            let (score, moves) = self.alpha_beta(
                &self.dynamics.apply_move(&board, &m),
                depth - 1,
                -beta,
                -max,
            );
            let selfish = -score;
            if selfish > max {
                max = selfish;
                best_moves = moves;
                best_moves.push_front(m);
            }
            if max >= beta {
                break;
            }
        }
        return (max, best_moves);
    }
    pub fn boost(&self, board: &D::State, depth: usize) -> (E::PlayerScore, VecDeque<D::Move>) {
        self.alpha_beta(board, depth, f64::NEG_INFINITY, f64::INFINITY)
    }
}

pub struct AlphaBetaLookup<D: Dynamics, E: Evaluator<Dyn = D>> {
    pub dynamics: D,
    pub evaluator: E,
    pub map: HashMap<D::State, D::Move>,
}

pub struct SearchContext<D: Dynamics, E: Eval, Ex: SearchExtend> {
    pub game: D,
    pub eval: E,
    pub extend: Ex,
    pub stop: Arc<AtomicBool>,
}


pub struct TransNode<Move> {
    depth: usize,
    score: SelfishScore<f64>,
    best_move: Move,
}

pub struct TranspositionTable<State, Move> {
    pub table: HashMap<State, TransNode<Move>>
}

pub fn search<D, E, Ex>(
    ctx: &SearchContext<D, E, Ex>,
    state: D::State,
    depth: usize,
    transpositions: &mut TranspositionTable<D::State, D::Move>,
) -> (SelfishScore<f64>, VecDeque<<D as Dynamics>::Move>)
where
    D: Dynamics,
    E: Eval<State = <D as Dynamics>::State, Score = SelfishScore<f64>>,
    Ex: SearchExtend<State = <D as Dynamics>::State, Move = <D as Dynamics>::Move>,
    <D as Dynamics>::State: Copy + Eq + std::hash::Hash,
    <D as Dynamics>::Move: Copy,
{
    struct SearchNode<'p, S> {
        board: S,
        prev: Option<&'p SearchNode<'p, S>>
    }
    fn go<D, E, Ex>(
        ctx: &SearchContext<D, E, Ex>,
        node: SearchNode<'_, D::State>,
        depth: usize,
        alpha: SelfishScore<f64>,
        beta: SelfishScore<f64>,
        transpositions: &mut TranspositionTable<D::State, D::Move>,
    ) -> (SelfishScore<f64>, VecDeque<<D as Dynamics>::Move>)
    where
        D: Dynamics,
        E: Eval<State = <D as Dynamics>::State, Score = SelfishScore<f64>>,
        Ex: SearchExtend<State = <D as Dynamics>::State, Move = <D as Dynamics>::Move>,
        <D as Dynamics>::State: Copy + Eq + std::hash::Hash,
        <D as Dynamics>::Move: Copy,
    {
        if ctx.stop.load(Ordering::Relaxed) {
            return (beta, VecDeque::new());
        }
        if depth <= 0 {
            return (ctx.eval.eval(&node.board), VecDeque::new());
        }

        let moves = ctx.game.available_moves(&node.board);
        if moves.is_empty() {
            return (ctx.eval.eval(&node.board), VecDeque::new());
        }

        let mut cur = &node;
        while let Some(n) = cur.prev {
            cur = n;
            if node.board == cur.board {
                //tracing::debug!("repetition, skip");
                return (SelfishScore(0.0), VecDeque::new());
            }
        }

        let mut max = alpha;
        let mut best_moves = VecDeque::from([moves[0]]);

        if let Some(trans) = transpositions.table.get(&node.board) {
            if trans.depth >= depth {
                return (trans.score, VecDeque::from([trans.best_move]));
            }
            let m = trans.best_move;
            let extend = ctx.extend.extend(&node.board, &m);
            let next_board = ctx.game.apply_move(&node.board, &m);
            let next_depth = if extend { depth } else { depth - 1 };
            let (score, mut moves) = go(
                ctx,
                SearchNode { board: next_board, prev: Some(&node) },
                next_depth,
                -beta,
                -max,
                transpositions,
            );
            if let Some(&m) = moves.get(0) {
                transpositions.table.insert(next_board, TransNode { score, best_move: m, depth: next_depth });
            }
            let selfish = -score;
            if selfish > max {
                max = selfish;
                moves.push_front(m);
                best_moves = moves;
            }
            if max >= beta {
                return (max, best_moves);
            }
        }

        for m in moves {
            let extend = ctx.extend.extend(&node.board, &m);
            let next_board = ctx.game.apply_move(&node.board, &m);
            let next_depth = if extend { depth } else { depth - 1 };
            let (score, mut moves) = go(
                ctx,
                SearchNode { board: next_board, prev: Some(&node) },
                if extend { depth } else { depth - 1 },
                -beta,
                -max,
                transpositions,
            );
            if let Some(&m) = moves.get(0) {
                transpositions.table.insert(next_board, TransNode { score, best_move: m, depth: next_depth });
            }
            let selfish = -score;
            if selfish > max {
                max = selfish;
                moves.push_front(m);
                best_moves = moves;
            }
            if max >= beta {
                break;
            }
        }
        return (max, best_moves);
    }

    go(
        ctx,
        SearchNode { board: state, prev: None },
        depth,
        SelfishScore(f64::NEG_INFINITY),
        SelfishScore(f64::INFINITY),
        transpositions,
    )
}

impl<S: Copy, P: Copy, M: Copy, B, D, E> AlphaBetaLookup<D, E>
where
    D: Dynamics<Player = P, Move = M, State = B>,
    E: Evaluator<Dyn = D, Score = S, PlayerScore = f64>,
    B: Copy + Eq + PartialEq + std::hash::Hash,
{
    fn alpha_beta(
        &mut self,
        board: &B,
        depth: usize,
        alpha: f64,
        beta: f64,
    ) -> (E::PlayerScore, VecDeque<D::Move>) {
        let player = self.dynamics.current_player(&board);
        if depth <= 0 {
            let score = self.evaluator.eval_for(&board, player);
            return (score, VecDeque::new());
        }
        let moves = self.dynamics.available_moves(&board);
        if moves.is_empty() {
            let score = self.evaluator.eval_for(&board, player);
            return (score, VecDeque::new());
        }

        let mut max = alpha;
        let mut best_moves = VecDeque::new();

        if let Some(&m) = self.map.get(&board) {
            let (score, moves) = self.alpha_beta(
                &self.dynamics.apply_move(&board, &m),
                depth - 1,
                -beta,
                -max,
            );
            let selfish = -score;
            if selfish > max {
                max = selfish;
                best_moves = moves;
                best_moves.push_front(m);
                self.map.insert(*board, m);
            }
            if max >= beta {
                return (max, best_moves);
            }
        }

        for m in moves {
            let (score, moves) = self.alpha_beta(
                &self.dynamics.apply_move(&board, &m),
                depth - 1,
                -beta,
                -max,
            );
            let selfish = -score;
            if selfish > max {
                max = selfish;
                best_moves = moves;
                best_moves.push_front(m);
                self.map.insert(*board, m);
            }
            if max >= beta {
                break;
            }
        }
        return (max, best_moves);
    }
    pub fn boost(&mut self, board: &B, depth: usize) -> (E::PlayerScore, VecDeque<D::Move>) {
        self.alpha_beta(board, depth, f64::NEG_INFINITY, f64::INFINITY)
    }
}
