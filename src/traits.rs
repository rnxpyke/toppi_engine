use std::ops::Neg;

pub trait Eval {
    type State;
    type Score;
    fn eval(&self, state: &Self::State) -> Self::Score;
}

pub trait SearchExtend {
    type State;
    type Move;
    fn extend(&self, state: &Self::State, mov: &Self::Move) -> bool;
}

#[derive(PartialEq, PartialOrd, Eq, Ord, Copy, Clone)]
pub struct SelfishScore<T>(pub T);

impl<T> Neg for SelfishScore<T>
where
    T: Neg,
{
    type Output = SelfishScore<<T as Neg>::Output>;

    fn neg(self) -> Self::Output {
        SelfishScore(-self.0)
    }
}
